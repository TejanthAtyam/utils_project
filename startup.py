import os
import sys
from IPython.display import display, HTML
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

HOME_DIR = os.getenv(f"HOME_DIR", None)
if HOME_DIR == None:
    raise Exception('NO HOME DIRECTORY HOME_DIR Environment variable')

DATA_DIR = os.getenv(f"DATA_DIR", None)
if DATA_DIR == None:
    raise Exception('NO DATA DIRECTORY DATA_DIR Environment variable')

UTILS_DIR = os.getenv(f"UTILS_DIR", None)
if UTILS_DIR == None:
    raise Exception('NO UTILS DIRECTORY UTILS_DIR Environment variable')

sys.path.append(UTILS_DIR)
from df_utils import *
# from postgres import * # moved to within df_utils
from hangouts_msg import *
from time_utils import *
from plotly_utils import *

def dirs(directory):
    """
    Report of values of _DIR variables
    Usage: dirs(dir())
    Output: list sorted by length, typically
    HOME_DIR  /...
    DATA_DIR  /.../data
    UTILS_DIR /.../utils
    """
    to_print = []
    for item in directory:
        if item.endswith("_DIR"):
            to_print.append( (item, eval(item)) )
    to_print_with_len = []
    for item in to_print:
        to_print_with_len.append( (item[0], item[1], len(item[1])) )
    for item in sorted(to_print_with_len, key=lambda x: x[2]):
        print(f"{item[0]:<9} {item[1]}")


if len(sys.argv) > 1:
    # if sys.argv[1] == 'silent':
    pass
else:
    print(f"Postgres environment loaded - {len(dbs)} DBs")
    dirs(dir())


def ff(string_in):
    """
    F-string Format (ff) returns an HTML version of the input, including bold (from <strong></strong> tags)
    @string_in: Usually an f-string with interpolated values, e.g. f'{len(df)} rows'
    """
    return HTML(string_in)
    

def get_current_data_dir_date_stamp():
    """
    Returns the path to the current data directory and appends the current timestamp
    Ex: 'DATA_DIR/YYYYMMDD/YYYYMMDD'
    """
    return f'{DATA_DIR}/{current_date()}/{current_date()}'

        
def has(dir_list, text):
    """
    Find text within any of the elements of dir_list and return matching elements.
    Skips elements starting with underscore _ character.
    has(dir(),'_') thus returns all elements with INTERNAL underscore.
    Usage:  has(dir(), 'show')
    Result: show_all_cols
    """
    for item in dir_list:
        if item.startswith('_'):
            continue
        if item.find(text) > -1:
            print(item)


def no_index(df):
    """
    Return HTML output of DataFrame with NO Index column shown
    @df: DataFrame to display
    """
    return display(HTML(df.to_html(index=False)))


def mkdir_if_not_exists(dir_name, debug=False):
    """
    Create target Directory if it doesn't exist
    """
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
        if debug:
            print("Directory " , dir_name ,  " Created ")
    else:    
        if debug:
            print("Directory " , dir_name ,  " already exists")


mkdir_if_not_exists(f'{DATA_DIR}/data_anomalies')
